#!/bin/bash -l
#SBATCH -A b2012114 
#SBATCH -p core -n 1
#SBATCH -t 04:00:00
#SBATCH -J mach2snptest

if [ $# -ne 4 ]
then
    echo "Usage: mach2snptest.sh prob_file info_file legend_file output_file"
    echo "Converts the given chunk to snptest format."
    exit 1
fi

prob_file=$1
info_file=$2
legend_file=$3
output_file=$4

convert_mach2snptest --legendfile $legend_file --rsq 0.0 --prefix $output_file --blocksize 1000000000 $prob_file $info_file
