#!/bin/bash
#SBATCH -A b2012114 
#SBATCH -p core -n 1
#SBATCH -t 02:00:00
#SBATCH -J merge_results

if [ $# -ne 3 ]
then
    echo "Usage: analyze_merge_to_chr.sh vcf_dir input_results output_dir"
    echo "Merges the chunk results into a single file per chromosome."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

# Vcf dir containing per chr information
vcf_dir=$1

# Directory containing the results
input_dir=$2

# Output dir
output_dir=$3

for chr in {1..22};
do
    input_chr_dir=$input_dir/chr/$chr
    output_chr_dir=$output_dir/chr/$chr
    mkdir -p $output_chr_dir
    
    output_file="$output_chr_dir/chr$chr.out"
    grep "^TRAIT" "$input_chr_dir/chunk1-chr$chr.out" > $output_file
    phenotype=`grep -A 1 "^TRAIT" "$input_chr_dir/chunk1-chr$chr.out" | tail -n 1 | awk '{ print $1 }'`

    chunk_list=`find $input_chr_dir -name "chunk*-chr$chr.out" | sort --version-sort`
    cat $chunk_list | grep "^$phenotype" >> $output_file

    #cat $chunk_list | grep "^$phenotype" >> $output_chr_dir/chr${chr}_pos.out
    #vcf_file=`find $vcf_dir -name "*chr$chr.out" | head -n 1`
    #paste $output_file $vcf_file > $output_chr_dir/chr${chr}_pos.out
done
