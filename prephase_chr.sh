#!/bin/bash -l
#SBATCH -A snic2013-1-170
#SBATCH -p core -n 2
#SBATCH -t 240:00:00
#SBATCH -J prephase_chr

if [ $# -ne 3 ]
then
    echo "Usage: prephase_chr merlin_prefix auto_chunk output_prefix"
    echo "Prephases a single chromosome and chunks it afterwards, the results are outputted in the given directory."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

# Path to the chunk
merlin_prefix=$1

# Path to the auto chunk file
auto_chunk=$2

# Path prefix for the output file
output_prefix=$3

mach1 -d $merlin_prefix.dat -p $merlin_prefix.ped --rounds 20 --states 200 --phase --interim 5 --sample 5 --prefix $output_prefix >& $output_prefix-mach.log
output_dir=$(dirname $output_prefix)
perl $IMP_DIR/scripts/chunkPhasedData.pl $merlin_prefix $auto_chunk $output_prefix.gz $output_dir
