#!/bin/bash -l
#SBATCH -A b2012114 
#SBATCH -p node -n 1
#SBATCH -t 02:00:00
#SBATCH -J analyze_chunk

if [ $# -ne 6 ]
then
    echo "Usage: analyze_chunk dose_type fam_file map_file dose_file covar_file phenotype_file output_file"
    echo "Converts a single minimac dose file to a plink dose file."
    exit 1
fi

dose_type=$1
fam_file=$2
map_file=$3
dose_file=$4
covar_file=$5
phenotype_file=$6
output_file=$7

format="format=1"
if [ $dose_type -eq "prob"  ];
then
    format="format=2"
fi

#plink --noweb --silent --fam $fam_file --dosage $dose_file Z $format --map $map_file --covar $covar_file --pheno $phenotype_file --out $output_file
mach2qtl --datfile $merlin_prefix.dat --pedfile $merlin_prefix.ped --infofile $chunk_prefix.info.gz --dosefile $chunk_prefix.dose.gz > $output_file
