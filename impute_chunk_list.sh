#!/bin/bash

if [ $# -ne 4 ]
then
    echo "Usage: impute refpanel_dir prephase_output_dir chunk_list_file output_dir"
    echo "Takes a list of chunks and imputes them. The format consists of lines containing first"
    echo "the chromosome number and then the path to the prephased chunk."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

# Directory containing the 1000G refpanels
refpanel_dir=$1

# Path to the output directory of the prephasing
prephase_dir=$2

# List of chunk files
chunk_list_file=$3

# Output directory for the imputation
output_dir=$4

while read chr chunk
do
    input_chr_dir=$prephase_dir/chr/$chr
    output_chr_dir=$output_dir/chr/$chr
    mkdir -p $output_chr_dir
    
    refpanel=$(find $refpanel_dir -maxdepth 1 -name "chr$chr.*.gz" | head -n 1)
    auto_clip_file=$input_chr_dir/autoChunk-chr$chr.merlin.dat
    
    chunk_name=$(basename "$chunk")
    chunk_name=${chunk_name%%.*}

    chunk_snps=${chunk%.*}.dat.snps
    sbatch $IMP_DIR/impute_chunk.sh $refpanel $chunk $chunk_snps $auto_clip_file $output_chr_dir/$chunk_name
done < $chunk_list_file
