#!/bin/bash

if [ $# -ne 3 ]
then
    echo "Usage: mach2snptest input_dir legend_dir output_dir"
    echo "Converts all files in the given imputation directory to snptest format."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

# Imputation directory
input_dir=$1

# Legend directory
legend_dir=$2

# Output directory 
output_dir=$3

for chr in {1..22};
do
    input_chr_dir=$input_dir/chr/$chr
    output_chr_dir=$output_dir/chr/$chr
    mkdir -p $output_chr_dir

    for chunk in $input_chr_dir/chunk*-chr$chr.imputed.prob.gz;
    do
        chunk_name=$(basename $chunk)
        chunk_name=${chunk_name%.*.*}

        chunk_prefix=${chunk%.*.*}

        legend_file=$legend_dir/legend_chr$chr.out

        info_file=$chunk_prefix.info.gz
        sbatch $IMP_DIR/mach2snptest_chunk.sh $chunk $info_file $legend_file $output_chr_dir/$chunk_name
    done
done

# X chromosome
chr='X'
for xtype in {males.auto,males.no.auto,females.auto,females.no.auto};
do
    input_chr_dir=$input_dir/chr/$chr
    output_chr_dir=$output_dir/chr/$chr
    mkdir -p $output_chr_dir

    chunk_list=($input_chr_dir/chunk*-chr$chr.$xtype.imputed.prob.gz)
    if [ ! -f ${chunk_list[0]} ];
    then
        continue
    fi

    for chunk in ${chunk_list[@]};
    do
        chunk_name=$(basename $chunk)
        chunk_name=${chunk_name%.*.*}

        chunk_prefix=${chunk%.*.*}
        legend_file=$legend_dir/legend_chrX.out

        info_file=$chunk_prefix.chrpos.info.gz
        sbatch $IMP_DIR/mach2snptest_chunk.sh $chunk $info_file $legend_file $output_chr_dir/$chunk_name
    done
done
