#!/bin/bash
#SBATCH -A snic2013-1-170
#SBATCH -p core -n 1
#SBATCH -t 1:00:00
#SBATCH -J chr_prephase

if [ $# -ne 4 ]
then
    echo "Usage: prephase_chrwise input_plink_base chunk_length chunk_overlap output_dir"
    echo "Prephases whole chromosomes, and chunks them afterwards. The results are outputted in the given directory."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

# Path prefix to the plink input files
plink_base=$1

# Number of SNPs in a chunk
length=$2

# Number of overlapping SNPs between chunks
overlap=$3

# Output directory
output_dir=$4

# Autosomes
for chr in {1..22};
do
    chr_dir=$output_dir/chr/$chr
    mkdir -p $chr_dir

    # Convert to merlin format and chunk chromosome
    $IMP_DIR/scripts/chr_to_merlin.sh $chr $plink_base $chr_dir/chr$chr
    pushd $chr_dir
    ChunkChromosome -d ./chr$chr.merlin.dat -n $length -o $overlap
    popd
    
    # Prephase each chunk as its own job
    merlin_prefix=$chr_dir/chr$chr.merlin
    auto_chunk=$chr_dir/autoChunk-chr$chr.merlin.dat
    sbatch $IMP_DIR/prephase_chr.sh $merlin_prefix $auto_chunk $merlin_prefix
done
