#!/bin/bash -l
#SBATCH -A b2012114 
#SBATCH -p core -n 1
#SBATCH -t 08:00:00
#SBATCH -J chunk_merge

if [ $# -le 1 ]
then
    echo "Usage: merge_chr output_file [chunk1 chunk2 ...]"
    echo "Merges the given chunks into a single big chunk."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

output_file=$1
shift
$IMP_DIR/merge_files.sh $* > $output_file
