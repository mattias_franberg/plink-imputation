#!/bin/bash

if [ $# -ne 3 ]
then
    echo "Usage: analyze sample_file chunk_dir output_dir"
    echo "Analyzes all chunks and outputs the results in the given directory."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

# snptest .sample file
sample_file=$1

# Directory containing all chunks in snptest format
chunk_dir=$2

# Output dir
output_dir=$3

for chr in {1..22};
do
    input_chr_dir=$chunk_dir/chr/$chr
    output_chr_dir=$output_dir/chr/$chr
    mkdir -p $output_chr_dir

    chr_gen_file=$input_chr_dir/chr$chr.gen.gz
    chr_sample_file=$input_chr_dir/chr$chr.sample

    sbatch $IMP_DIR/analyze_chunk_snptest.sh $sample_file $chr_gen_file $chr_sample_file $output_chr_dir/chr$chr.out    
done

# X chromosome
#chr="X"
#for chr_type in {males.auto,males.no.auto,females.auto,females.no.auto}
#do
#    input_chr_dir=$transposed_dir/chr/$chr
#    output_chr_dir=$output_dir/chr/$chr
#    mkdir -p $output_chr_dir
#    
#    num_chunks=$( find $input_chr_dir -name "chunk*-chr$chr.$chr_type.imputed.transposed.$dose_type.gz" | wc -l )
#    if [ $num_chunks -gt 0 ]; then
#        for chunk in $input_chr_dir/chunk*-chr$chr.$chr_type.imputed.transposed.$dose_type.gz;
#        do
#            chunk_name=$(basename "$chunk")
#            chunk_name=${chunk_name%%.*}
#            
#            sbatch $IMP_DIR/analyze_chunk.sh $dose_type $fam_file $map_file $chunk $covar_file $pheno_file $output_chr_dir/$chunk_name
#        done
#    fi
#done
