import sys
import xlrd

def clean_name(column_name):
    return unicode( column_name.strip( ).replace( "i:", "" ).lower( ) )

def clean_cell(cell):
    return clean_name( cell.value )

def get_conversion(name):
    if name.startswith( "i:" ):
        return int_str
    else:
        return str

def int_str(value):
    return str( int( value ) )

if len( sys.argv ) < 2:
    print( "Usage: extract_column excel_file col1 col2 col3" )
    exit( )

print_only = False
if len( sys.argv ) == 2:
    print_only = True

excel_file = sys.argv[ 1 ]

book = xlrd.open_workbook( excel_file )
sheet = book.sheets( )[ 0 ]
if print_only:
    for cell in sheet.row( 0 ):
        print( cell.value )

    exit( 0 )
    
column_list = list( map( clean_name, sys.argv[ 2: ] ) )
excel_columns = list( map( clean_cell, sheet.row( 0 ) ) )
conversion = list( map( get_conversion, sys.argv[ 2: ] ) )

name_to_index = dict( )
for index, column in enumerate( excel_columns ):
    name_to_index[ column ] = index

output_columns = list( )
for column in column_list:
    index = name_to_index.get( column, -1 )
    if index != -1:
        output_columns.append( sheet.col( index ) )
    else:
        print( "Warning: Could not find column '{0}'.".format( column ) )

if len( output_columns ) <= 0:
    print( "No columns found" )
    exit( 1 )

header = [ str( column[ 0 ].value ) for column in output_columns ]
print( '\t'.join( header ) )
for row in range( 1, sheet.nrows ):
    values = [ conversion[ i ]( column[ row ].value ) for i, column in enumerate( output_columns ) ]
    print( '\t'.join( values ) )
