#!/bin/bash

if [ $# -ne 7 ]
then
    echo "Usage: analyze dose_type fam_file map_file covar_file phenotype_file transposed_dir output_dir"
    echo "Analyzes all chunks and outputs the results in the given directory."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

# Type of dose file .prob or .dose
dose_type=$1

# Fam file
fam_file=$2

# Map file
map_file=$3

# Covariates
covar_file=$4

# Phenotype
pheno_file=$5

# Transposed dir
transposed_dir=$6

# Output dir
output_dir=$7

for chr in {22..22};
do
    input_chr_dir=$transposed_dir/chr/$chr
    output_chr_dir=$output_dir/chr/$chr
    mkdir -p $output_chr_dir
    
    for chunk in $input_chr_dir/chunk*-chr$chr.imputed.transposed.$dose_type.gz;
    do
        chunk_name=$(basename "$chunk")
        chunk_name=${chunk_name%%.*}

        $IMP_DIR/analyze_chunk.sh $dose_type $fam_file $map_file $chunk $covar_file $pheno_file $output_chr_dir/$chunk_name
    done
done

# X chromosome
#chr="X"
#for chr_type in {males.auto,males.no.auto,females.auto,females.no.auto}
#do
#    input_chr_dir=$transposed_dir/chr/$chr
#    output_chr_dir=$output_dir/chr/$chr
#    mkdir -p $output_chr_dir
#    
#    num_chunks=$( find $input_chr_dir -name "chunk*-chr$chr.$chr_type.imputed.transposed.$dose_type.gz" | wc -l )
#    if [ $num_chunks -gt 0 ]; then
#        for chunk in $input_chr_dir/chunk*-chr$chr.$chr_type.imputed.transposed.$dose_type.gz;
#        do
#            chunk_name=$(basename "$chunk")
#            chunk_name=${chunk_name%%.*}
#            
#            sbatch $IMP_DIR/analyze_chunk.sh $dose_type $fam_file $map_file $chunk $covar_file $pheno_file $output_chr_dir/$chunk_name
#        done
#    fi
#done
