#!/bin/bash

# Determines which chromosomes to prephase
chr_list=$(seq 1 22)

# Determines whether or not to prephase autosomes
do_auto=1

# Determines whether or not to prephase x-chromosome
do_x=1

# Do QTL analysis instead of case/control
qtl_flag=""

# Ignore covariates
cov_flag=""
while getopts c:r:xaqn flag; do
    case $flag in
        q)
            qtl_flag="-q"
            ;;
        n)
            cov_flag="-c"
            ;;
        a)
            do_auto=0
            ;;
        x)
            do_x=0
            ;;
        ?)
            exit;
            ;;
    esac
done

shift $(( OPTIND - 1 ));

if [ $# -ne 5 ]
then
    echo "Usage: analyze [-c chr_to_analyze] [-axqn] dose_type cond_sets cond_dir chunk_dir output_dir"
    echo "Analyzes all chunks and outputs the results in the given directory."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

# Type of dose file .prob or .dose
dose_type=$1

# List of conditional analyses to perform
cond_sets=$2

# Directory with extracted conditional data
cond_dir=$3

# Directory containing all chunks
chunk_dir=$4

# Output dir
output_dir=$5

while read locus set chr;
do
    input_chr_dir=$chunk_dir/chr/$chr
    output_chr_dir=$output_dir/$locus/$set
    mkdir -p $output_chr_dir
    pheno_covar_prefix="${cond_dir}_${locus}_${set}"
    
    for chunk in $input_chr_dir/chunk*-chr$chr.imputed.$dose_type.gz;
    do
        chunk_prefix=${chunk%.*.*}

        chunk_name=$(basename "$chunk")
        chunk_name=${chunk_name%%.*}

        sbatch $IMP_DIR/analyze_chunk_mach.sh $qtl_flag $cov_flag $dose_type $pheno_covar_prefix $chunk_prefix $output_chr_dir/$chunk_name.out
    done
done < <(cat $cond_sets | awk '{ print $1, $2, $5 }' | uniq)
