#!/bin/bash
#SBATCH -A b2012114 
#SBATCH -p core -n 1
#SBATCH -t 00:30:00
#SBATCH -J manhattan_plot

if [ $# -ne 4 ]
then
    echo "Usage: run_manhattan.sh mach.out pvalue_column title output.png"
    echo "Makes a manhattan plot for a chromosome."
    exit 1
fi

Rscript $IMP_DIR/manhattan/manhattan_plot.r $1 $2 "$3" $4
