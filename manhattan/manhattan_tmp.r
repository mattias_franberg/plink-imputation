argv = commandArgs( trailingOnly = T )
if( length( argv ) != 3 )
{
    print( "Usage: manhattan_plot mach2qtl_data title output_pdf" )
    q( )
}

all_args = commandArgs( trailingOnly = F )  
script_path = dirname( sub( "--file=", "", all_args[ grep( "--file", all_args ) ] ) )
qqman_path = paste( script_path, "qqman.r", sep = "/" )
source( qqman_path )

mach2qtl_file = argv[ 1 ]
title = argv[ 2 ]
output_file = argv[ 3 ]

mach2qtl_data = read.table( mach2qtl_file, header = T )
mach2qtl_data = subset( mach2qtl_data, select = c( id, chromosome, pos, phe_frequentist_add_sex_age_c1_c2_c3_c4_score_pvalue ) )
names( mach2qtl_data ) = c( "SNP", "CHR", "BP", "P" )
mach2qtl_data$CHR = 9
mach2qtl_data = subset( mach2qtl_data, mach2qtl_data$P != -1 )

png( output_file, width = 1024, height = 768 )

manhattan( mach2qtl_data, pch=20, genomewideline=F, suggestiveline=F, main = title )

dev.off( )
