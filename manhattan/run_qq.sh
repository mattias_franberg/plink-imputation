#!/bin/bash
#SBATCH -A b2012114 
#SBATCH -p core -n 1
#SBATCH -t 00:30:00
#SBATCH -J qq_plot

if [ $# -ne 2 ]
then
    echo "Usage: run_qq mach2.out output.png"
    echo "Makes a qq-plot for a chromosome\n"
    exit 1
fi

Rscript $IMP_DIR/manhattan/qq.r $1 $2
