#!/bin/bash
#SBATCH -A b2012114 
#SBATCH -p core -n 1
#SBATCH -t 00:15:00
#SBATCH -J rsq_plot

if [ $# -ne 2 ]
then
    echo "Usage: run_rsq mach2.out output.png"
    echo "Makes a cumulative distribution plot of R squared.\n"
    exit 1
fi

Rscript $IMP_DIR/manhattan/rsq.r $1 $2
