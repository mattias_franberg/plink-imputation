library( ggplot2 )

argv = commandArgs( trailingOnly = T )
if( length( argv ) != 2 )
{
    print( "Usage: manhattan_plot mach2qtl_data output_png" )
    q( )
}

all_args = commandArgs( trailingOnly = F )  
script_path = dirname( sub( "--file=", "", all_args[ grep( "--file", all_args ) ] ) )
qqman_path = paste( script_path, "qqman.r", sep = "/" )
source( qqman_path )

mach2qtl_file = argv[ 1 ]
output_file = argv[ 2 ]

mach2qtl_data = read.table( mach2qtl_file, header = T )
names( mach2qtl_data ) = c( "SNP", "CHR", "BP", "PVALUE" )

gc = median( qchisq( mach2qtl_data$PVALUE, df=1, lower.tail=F ), na.rm=T ) / 0.456

png( output_file, width = 1024, height = 768 )
qq( mach2qtl_data$PVALUE, main = paste( "QQ with GC =", gc ) )
dev.off( )
