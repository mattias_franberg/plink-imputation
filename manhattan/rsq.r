library( ggplot2 )

argv = commandArgs( trailingOnly = T )
if( length( argv ) != 2 )
{
    print( "Usage: rsq mach2qtl_data output_png" )
    q( )
}

mach2qtl_file = argv[ 1 ]
output_file = argv[ 2 ]

mach2qtl_data = read.table( mach2qtl_file, header = T )

# Fix invalid mach2dat output
mach2qtl_data$RSQR = as.character( mach2qtl_data$RSQR )
mach2qtl_data$RSQR[ mach2qtl_data$RSQR == ".-000" ] = ".0000"
mach2qtl_data$RSQR = as.numeric( mach2qtl_data$RSQR )

png( output_file, width = 1024, height = 768 )
ggplot( mach2qtl_data, aes( RSQR ) ) + stat_ecdf() + 
    ggtitle( "R squared cumulative distribution" ) + 
    scale_x_continuous( "R squared", limits = c( 0.0, 1.0 ) ) + 
    scale_y_continuous( "% less than corresponding R squared")
dev.off( )
