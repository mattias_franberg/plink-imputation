argv = commandArgs( trailingOnly = T )
if( length( argv ) != 4 )
{
    print( "Usage: manhattan_plot mach2qtl_data pvalue_column title output_png" )
    q( )
}

all_args = commandArgs( trailingOnly = F )  
script_path = dirname( sub( "--file=", "", all_args[ grep( "--file", all_args ) ] ) )
qqman_path = paste( script_path, "qqman.r", sep = "/" )
source( qqman_path )

mach2qtl_file = argv[ 1 ]
pvalue_column = argv[ 2 ]
title = argv[ 3 ]
output_file = argv[ 4 ]

mach2qtl_data = read.table( mach2qtl_file, header = T )
mach2qtl_data = subset( mach2qtl_data, select = c( "rsid", "chromosome", "position", pvalue_column ) )
names( mach2qtl_data ) = c( "SNP", "CHR", "BP", "P" )

png( output_file, width = 1024, height = 768 )

manhattan( mach2qtl_data, pch=20, genomewideline=-log10(5e-8), suggestiveline=F, main = title )

dev.off( )
