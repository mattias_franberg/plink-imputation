#!/bin/bash -l
#SBATCH -A b2012114 
#SBATCH -p core -n 1
#SBATCH -t 02:00:00
#SBATCH -J chunk_transpose

if [ $# -ne 6 ]
then
    echo "Usage: transpose_chunk type alias_file order_file dose_file info_file output_file"
    echo "Transposes the given chunk."
    exit 1
fi

dose_type=$1
alias_file=$2
order_file=$3
dose_file=$4
info_file=$5
output_file=$6

chunk_name=$(basename $output_file)
output_dir=$(dirname $output_file)

echo "Transposing: $dose_file"
dose_to_plink -t $dose_type --order-file $order_file -d $dose_file -i $info_file -o $TMPDIR/$chunk_name

cp $TMPDIR/$chunk_name $output_file
cp $info_file $output_dir/
