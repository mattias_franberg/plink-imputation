#!/bin/bash

if [ $# -ne 5 ]
then
    echo "Usage: merge_cond_mach prefix postfix cond_sets res_dir output_dir"
    echo "Analyzes all chunks and outputs the results in the given directory."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

prefix=$1

postfix=$2

# List of conditional analyses to perform
cond_sets=$3

# Directory that contains the results
res_dir=$4

# Output dir
output_dir=$5

while read locus set chr;
do
    input_dir=$res_dir/$locus/$set
    result_list=$(ls $input_dir/*.out | sort --version-sort)
    first_chunk=$(ls $input_dir/*.out | sort --version-sort | head -n 1)

    output_file="$output_dir/${prefix}_${locus}_${set}_$postfix"
    mkdir -p $output_dir 

    grep "^TRAIT" "$first_chunk" > $output_file
    phenotype=`grep -A 1 "^TRAIT" "$first_chunk" | tail -n 1 | awk '{ print $1 }'`
    cat $result_list | grep "^$phenotype" >> $output_file
    
done < <(cat $cond_sets | awk '{ print $1, $2, $5 }' | uniq)
