#!/bin/bash

if [ $# -ne 3 ]
then
    echo "Usage: merge type input_dir output_dir"
    echo "Merges chunks into whole chromosome pieces."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

# Dose type 'prob' or 'dose'
dose_type=$1

# Input directory
input_dir=$2

# Output directory 
output_dir=$3

for chr in {1..22};
do
    input_chr_dir=$input_dir/chr/$chr
    output_chr_dir=$output_dir/chr/$chr
    mkdir -p $output_chr_dir

    chunk_list=`ls $input_chr_dir/chunk*-chr$chr.imputed.transposed.$dose_type.gz | sort --version-sort`
    chunk_array=($chunk_list)

    if [ ! -f ${chunk_array[0]} ];
    then
        continue
    fi

    $IMP_DIR/merge_chr.sh $output_chr_dir/chr$chr.imputed.transposed.$dose_type.gz $chunk_list
done

# X chromosome
chr='X'
for xtype in {males.auto,males.no.auto,females.auto,females.no.auto};
do
    input_chr_dir=$input_dir/chr/$chr
    output_chr_dir=$output_dir/chr/$chr
    mkdir -p $output_chr_dir

    chunk_list=`ls $input_chr_dir/chunk*-chr$chr.$xtype.imputed.transposed.$dose_type.gz | sort --version-sort`
    chunk_array=($chunk_list)

    if [ ! -f ${chunk_array[0]} ];
    then
        continue
    fi

    sbatch $IMP_DIR/merge_chr.sh $output_chr_dir/chr$chr.$xtype.imputed.transposed.$dose_type.gz $chunk_list
done
