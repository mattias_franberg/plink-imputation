#!/bin/bash

# Determines which chromosomes to prephase
chr_list=$(seq 1 22)

# Determines whether or not to prephase autosomes
do_auto=1

# Determines whether or not to prephase x-chromosome
do_x=1

# Path to the alias file
alias_flag=""
while getopts c:r:xa flag; do
    case $flag in
        c)
            chr_list=$OPTARG
            ;;
        a)
            do_auto=0
            ;;
        x)
            do_x=0
            ;;
        r)
            alias_flag="-r $OPTARG"
            ;;
        ?)
            exit;
            ;;
    esac
done

shift $(( OPTIND - 1 ));
if [ $# -ne 3 ]
then
    echo "Usage: impute [-c list_of_chr_to_impute] [-ax] [-r alias_file] refpanel_dir prephase_output_dir output_dir"
    echo "Imputes all chunks and outputs the results in the given directory."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

# Directory containing the 1000G refpanels
refpanel_dir=$1

# Path to the output directory of the prephasing
prephase_dir=$2

# Output directory for the imputation
output_dir=$3

if [ $do_auto == 1 ]; then
    for chr in $chr_list;
    do
        input_chr_dir=$prephase_dir/chr/$chr
        output_chr_dir=$output_dir/chr/$chr
        mkdir -p $output_chr_dir
        
        refpanel=$(find $refpanel_dir -maxdepth 1 -name "chr$chr.*.gz" | head -n 1)
        auto_clip_file=$input_chr_dir/autoChunk-chr$chr.merlin.dat

        for chunk in $input_chr_dir/chunk*-chr$chr.merlin.gz;
        do
            chunk_name=$(basename "$chunk")
            chunk_name=${chunk_name%%.*}

            chunk_snps=${chunk%.*}.dat.snps
            sbatch $IMP_DIR/impute_chunk.sh $alias_flag $refpanel $chunk $chunk_snps $auto_clip_file $output_chr_dir/$chunk_name
        done
    done
fi

# X chromosome
if [ $do_x == 1 ]; then
    chr="X"
    for sex in {males,females}
    do
        input_chr_dir=$prephase_dir/chr/$chr
        output_chr_dir=$output_dir/chr/$chr
        mkdir -p $output_chr_dir
        
        refpanel_auto=$(find $refpanel_dir -maxdepth 1 -name "chr$chr.auto*.gz" | head -n 1)
        refpanel_no_auto=$(find $refpanel_dir -maxdepth 1 -name "chr$chr.no.auto*.gz" | head -n 1)
        auto_clip_file=$input_chr_dir/autoChunk-chr$chr.$sex.merlin.dat

        num_chunks=$( find $input_chr_dir -name "chunk*-chr$chr.$sex.merlin.gz" | wc -l )
        if [ $num_chunks -gt 0 ]; then
            for chunk in $input_chr_dir/chunk*-chr$chr.$sex.merlin.gz;
            do
                chunk_name=$(basename "$chunk")
                chunk_name=${chunk_name%%.*}

                chunk_snps=${chunk%.*}.dat.snps
                sbatch $IMP_DIR/impute_chunk.sh $alias_flag $refpanel_auto $chunk $chunk_snps $auto_clip_file $output_chr_dir/$chunk_name.$sex.auto
                sbatch $IMP_DIR/impute_chunk.sh $alias_flag $refpanel_no_auto $chunk $chunk_snps $auto_clip_file $output_chr_dir/$chunk_name.$sex.no.auto
            done
        fi
    done
fi
