#!/bin/bash

if [ $# -ne 5 ]
then
    echo "Usage: analyze_list_mach dose_type pheno_covar_prefix chunk_dir chunk_list output_dir"
    echo "Analyzes all chunks and outputs the results in the given directory."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

# Type of dose file .prob or .dose
dose_type=$1

# Prefix for .dat and .ped files
pheno_covar_prefix=$2

# Directory containing all chunks
chunk_dir=$3

# List of chunks to do
chunk_list=$4

# Output dir
output_dir=$5

while read chr chunk;
do
    input_chr_dir=$chunk_dir/chr/$chr
    output_chr_dir=$output_dir/chr/$chr
    mkdir -p $output_chr_dir
    
    chunk_prefix=${chunk%.*.*}

    chunk_name=$(basename "$chunk")
    chunk_name=${chunk_name%%.*}

    sbatch $IMP_DIR/analyze_chunk_mach.sh -qc $dose_type $pheno_covar_prefix $chunk_prefix $output_chr_dir/$chunk_name.out
done < $chunk_list
