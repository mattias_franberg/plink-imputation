#!/bin/bash -l
#SBATCH -A b2012114
#SBATCH -p core -n 4
#SBATCH -t 120:00:00
#SBATCH -J chunk_imputation

alias_file=""
while getopts r: flag; do
    case $flag in
        r)
            alias_file=$OPTARG
            ;;
        ?)
            exit;
            ;;
    esac
done

shift $(( OPTIND - 1 ));

if [ $# -ne 5 ]
then
    echo "Usage: impute_chunk [-r alias_file] ref.vcf chunk_file chunk_snps auto_clip_file output_prefix"
    echo "Imputes a single chunk and outputs it in the given directory."
    exit 1
fi

ref_vcf=$1
chunk_file=$2
chunk_snps=$3
auto_clip_file=$4
output_prefix=$5

echo "Imputing: $chunk_file $output_prefix"

if [[ $alias_file != "" ]];
then
    minimac-omp --cpus 4 --gzip --probs --refHaps $ref_vcf --vcfReference --rs --snpAliases $alias_file --haps $chunk_file --snps $chunk_snps --autoClip $auto_clip_file --prefix $output_prefix.imputed >& $output_prefix-minimac.log
else
    minimac-omp --cpus 4 --gzip --probs --refHaps $ref_vcf --vcfReference --haps $chunk_file --snps $chunk_snps --autoClip $auto_clip_file --prefix $output_prefix.imputed >& $output_prefix-minimac.log
fi
