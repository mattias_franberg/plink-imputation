#!/bin/bash -l
#SBATCH -A snic2013-1-170 
#SBATCH -p core -n 1
#SBATCH -t 72:00:00
#SBATCH -J chunk_prephase

if [ $# -ne 3 ]
then
    echo "Usage: prephase_chunk chunk.dat chr.ped output_prefix"
    echo "Prephases a single chunk and outputs it in the given directory."
    exit 1
fi

# Path to the chunk
chunk_path=$1

# Path to the .ped for the chromosome
chr_path=$2

# Path prefix for the output file
prefix=$3

mach1 -d $chunk_path -p $chr_path --rounds 20 --states 200 --phase --interim 5 --sample 5 --prefix $prefix >& $prefix-mach.log
