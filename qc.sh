#!/bin/bash

if [ $# -ne 2 ]
then
    echo "Usage: qc.sh input_results output_dir"
    echo "Produces QC information for the given results."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

# Directory containing the results
input_dir=$1

# Output dir
output_dir=$2

for chr in {1..22};
do
    input_chr_dir=$input_dir/chr/$chr
    output_chr_dir=$output_dir/chr/$chr
    mkdir -p $output_chr_dir
  
    sbatch $IMP_DIR/manhattan/run_qq.sh $output_chr_dir/chr$chr.out $output_chr_dir/chr${chr}_qq.png
    sbatch $IMP_DIR/manhattan/run_rsq.sh $output_chr_dir/chr$chr.out $output_chr_dir/chr${chr}_rsq.png
    sbatch $IMP_DIR/manhattan/run_manhattan.sh $output_chr_dir/chr${chr}_pos.out "Chromosome $chr" $output_chr_dir/chr${chr}_manhattan.png
done
