#!/bin/bash -l 
#SBATCH -A b2012114
#SBATCH -p core
#SBATCH -n 1
#SBATCH -t 8:00:00
#SBATCH -J analyze_chunk

# Path to the alias file
do_qtl=0
cov_flag="--useCovariates:OFF"
while getopts qc flag; do
    case $flag in
        q)
            do_qtl=1
            ;;
        c)
            cov_flag=""
            ;;
        ?)
            exit;
            ;;
    esac
done

shift $(( OPTIND - 1 ));
if [ $# -ne 4 ]
then
    echo "Usage: analyze_chunk dose_type pheno_covar_prefix chunk_prefix output_file"
    echo "Converts a single minimac dose file to a plink dose file."
    exit 1
fi

dose_type=$1
pheno_covar_prefix=$2
chunk_prefix=$3
output_file=$4

if [ $do_qtl == 1 ]; then
    mach2qtl --datfile $pheno_covar_prefix.dat --pedfile $pheno_covar_prefix.ped --infofile $chunk_prefix.info.gz --dosefile $chunk_prefix.$dose_type.gz $cov_flag --samplesize > $output_file
else
    mach2dat --datfile $pheno_covar_prefix.dat --pedfile $pheno_covar_prefix.ped --infofile $chunk_prefix.info.gz --dosefile $chunk_prefix.$dose_type.gz --verboseSampleSize --frequency > $output_file
fi
