#!/bin/bash -l 
#SBATCH -A b2012114
#SBATCH -p core -n 1
#SBATCH -t 30:00:00
#SBATCH -J analyze_chunk

if [ $# -ne 4 ]
then
    echo "Usage: analyze_chunk global_sample_file chunk_gen_file chunk_sample_file output_file"
    echo "Analyzes a chunk with snptest."
    exit 1
fi

sample_file=$1
chunk_gen_file=$2
chunk_sample_file=$3
output_file=$4

merged_sample_file=$output_file.sample

python $IMP_DIR/scripts/merge_snptest_sample.py $chunk_sample_file $sample_file > $merged_sample_file
snptest_v2.5-beta4 -data $chunk_gen_file $merged_sample_file -hwe -frequentist 1 2 3 -method score -pheno lnFXI -cov_all -chunk 10000 -o $output_file &> $output_file.log
