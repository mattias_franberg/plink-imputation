#!/bin/bash
#SBATCH -A b2013089
#SBATCH -p core -n 1
#SBATCH -t 1:00:00
#SBATCH -J shapeit_imputation

if [ $# -ne 5 ]
then
    echo "Usage: impute chr_length map_dir ref_dir input_dir output_dir"
    echo "Imputes the chunks. The results are outputted in the given directory."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

chr_length=$1

# Contains the genetic map files
map_dir=$2

# Contains the reference panels
ref_dir=$3

# Contains prephased chromosomes
input_dir=$4

# Output directory
output_dir=$5

# Autosomes
while read chr length
do
    chr_output_dir=$output_dir/chr/$chr
    mkdir -p $chr_output_dir
    
    chr_input_dir=$input_dir/chr/$chr
    phased_prefix=$chr_input_dir/chr$chr

    map_file=$(find $map_dir -maxdepth 1 -name "genetic_map_chr${chr}_*.txt" | head -n 1)
    ref_file=$(find $ref_dir -maxdepth 1 -name "*_chr${chr}_*.hap.gz" | head -n 1)
    ref_prefix=${ref_file%.*.*}

    step=5000000
    pos_start=0
    for ((pos_start = 0 ; pos_start < length ; pos_start += step+1 ));
    do
        pos_end=$((pos_start+step))

        if [[ ! -e $phased_prefix.hap.gz ]]; then
            continue
        fi

        sbatch $IMP_DIR/impute2/impute1000g_chunk.sh $phased_prefix $map_file $ref_prefix $pos_start $pos_end $chr_output_dir/chr${chr}_chunk_${pos_start}_${pos_end}
    done
done < $chr_length
