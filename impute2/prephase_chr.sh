#!/bin/bash -l
#SBATCH -A b2012114
#SBATCH -p node -n 1
#SBATCH -t 4:00:00
#SBATCH -J shapeit_prephase

if [ $# -ne 3 ]
then
    echo "Usage: prephase_chr plink_chr map_file output_prefix"
    echo "Prephases a single chromosome and outputs it in the given directory."
    exit 1
fi

# Path to the chunk
plink_chr=$1

# Path to the .ped for the chromosome
map_file=$2

# Path prefix for the output file
output_prefix=$3

shapeit --thread 16 --window 2 --states 200 --effective-size 11418 -B $plink_chr --input-map $map_file --output-log $output_prefix.log --output-max $output_prefix.hap.gz $output_prefix.sample
