#!/bin/bash
#SBATCH -A b2012114
#SBATCH -p core -n 1
#SBATCH -t 1:00:00
#SBATCH -J shapeit_prepare

if [ $# -ne 3 ]
then
    echo "Usage: prephase_chrwise input_plink_base map_dir output_dir"
    echo "Prephases whole chromosomes, and chunks them afterwards. The results are outputted in the given directory."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

# Path prefix to the plink input files
plink_base=$1

# Contains the genetic map files
map_dir=$2

# Output directory
output_dir=$3

# Autosomes
for chr in {1..22};
do
    chr_dir=$output_dir/chr/$chr
    mkdir -p $chr_dir

    # Convert to merlin format and chunk chromosome
    plink_chr=$chr_dir/plink_chr$chr
    plink2 --bfile $plink_base --chr $chr --make-bed --out $plink_chr

    map_file=$(find $map_dir -maxdepth 1 -name "genetic_map_chr${chr}_*.txt" | head -n 1)
    sbatch $IMP_DIR/impute2/prephase_chr.sh $plink_chr $map_file $chr_dir/chr$chr
done

chr='X'

chr_dir=$output_dir/chr/$chr
mkdir -p $chr_dir

# Convert to merlin format and chunk chromosome
plink_chr=$chr_dir/plink_chr$chr
plink2 --bfile $plink_base --chr $chr --make-bed --out $plink_chr

map_file=$(find $map_dir -maxdepth 1 -name "genetic_map_chrX_nonPAR*.txt" | head -n 1)
sbatch $IMP_DIR/impute2/prephase_chr.sh $plink_chr $map_file $chr_dir/chr$chr
