#!/bin/bash
#SBATCH -A b2013089
#SBATCH -p core -n 1
#SBATCH -t 2:00:00
#SBATCH -J shapeit_imputation

if [ $# -ne 4 ]
then
    echo "Usage: impute map_dir ref_dir input_dir output_dir"
    echo "Imputes the chunks. The results are outputted in the given directory."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

# Contains the genetic map files
map_dir=$1

# Contains the reference panels
ref_dir=$2

# Contains prephased chromosomes
input_dir=$3

# Output directory
output_dir=$4

# Autosomes
for chr in {6..6};
do
    chr_output_dir=$output_dir/chr/$chr
    mkdir -p $chr_output_dir
    
    chr_input_dir=$input_dir/chr/$chr
    hap_file=$chr_input_dir/chr$chr.hap.gz
    sample_file=$chr_input_dir/chr$chr.sample

    map_file=$(find $map_dir -maxdepth 1 -name "genetic_map_chr${chr}_*.txt" | head -n 1)
    chunk_list=$(find $ref_dir -maxdepth 1 -name "chr$chr.*shapeit.hap.gz")
    for chunk in $chunk_list;
    do
        ref_prefix=${chunk%.*.*}

        chunk_name=$(basename $chunk)
        chunk_name=${chunk_name%.*.*.*}
    
        sbatch $IMP_DIR/impute2/impute_chunk.sh $map_file $ref_prefix $hap_file $sample_file $chr_output_dir/$chunk_name
    done
done
