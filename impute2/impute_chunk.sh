#!/bin/bash -l
#SBATCH -A b2012114 
#SBATCH -p core -n 2
#SBATCH -t 48:00:00
#SBATCH -J chunk_imputation

if [ $# -ne 5 ]
then
    echo "Usage: impute_chunk map_file ref_prefix hap_file sample_file output_prefix"
    echo "Imputes a single chunk and outputs it in the given directory."
    exit 1
fi

map_file=$1
ref_prefix=$2
hap_file=$3
sample_file=$4
output_prefix=$5

echo "Imputing chunk: $ref_prefix"

# Find range
range=($(grep -o "\-int [0-9]\+ [0-9]\+" $ref_prefix.cmd | awk '{ print $2, $3}'))

impute2 -allow_large_regions -m $map_file -h $ref_prefix.hap.gz -l $ref_prefix.legend.gz -known_haps_g $hap_file -sample_g $sample_file -use_prephased_g -k_hap 10000 -int ${range[0]} ${range[1]} -Ne 20000 -buffer 250 -o $output_prefix.gen >& $output_prefix.log
gzip $output_prefix.gen
