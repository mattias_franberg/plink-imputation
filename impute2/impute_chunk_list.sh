#!/bin/bash

if [ $# -ne 5 ]
then
    echo "Usage: impute map_dir ref_dir input_dir chunk_list_file output_dir"
    echo "Takes a list of chunks and imputes them. The format consists of lines containing first"
    echo "the chromosome number and then the path to the prephased chunk."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

# Contains the genetic map files
map_dir=$1

# Contains the reference panels
ref_dir=$2

# Contains prephased chromosomes
input_dir=$3

# List of chromosome and chunk files
chunk_list_file=$4

# Output directory for the imputation
output_dir=$5

while read chr chunk
do
    chr_output_dir=$output_dir/chr/$chr
    mkdir -p $chr_output_dir
    
    chr_input_dir=$input_dir/chr/$chr
    hap_file=$chr_input_dir/chr$chr.hap.gz
    sample_file=$chr_input_dir/chr$chr.sample

    map_file=$(find $map_dir -maxdepth 1 -name "genetic_map_chr${chr}_*.txt" | head -n 1)
    
    ref_prefix=$chunk

    chunk_name=$(basename $chunk)
    chunk_name=${chunk_name%.*}
    
    sbatch $IMP_DIR/impute2/impute_chunk.sh $map_file $ref_prefix $hap_file $sample_file $chr_output_dir/$chunk_name
done < $chunk_list_file
