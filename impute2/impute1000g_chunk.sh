#!/bin/bash -l
#SBATCH -A b2012114 
#SBATCH -p core -n 1
#SBATCH -t 48:00:00
#SBATCH -J chunk_imputation

if [ $# -ne 6 ]
then
    echo "Usage: impute_chunk phased_prefix map_file ref_prefix pos_start pos_end output_prefix"
    echo "Imputes a single chunk and outputs it in the given directory."
    exit 1
fi

phased_prefix=$1
map_file=$2
ref_prefix=$3
pos_start=$4
pos_end=$5
output_prefix=$6

echo "Imputing chunk: $phased_prefix $pos_start $pos_end"

impute2 -m $map_file -h $ref_prefix.hap.gz -l $ref_prefix.legend.gz -known_haps_g $phased_prefix.hap.gz -sample_g $phased_prefix.sample -use_prephased_g -k_hap 500 -int $pos_start $pos_end -Ne 20000 -buffer 250 -o $output_prefix.gen >& $output_prefix.log
gzip $output_prefix.gen
