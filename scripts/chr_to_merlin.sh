#!/bin/bash

if [ $# -ne 3 ]
then
    echo "Usage: chr_to_merlin chr input_base output_base"
    echo "Extracts the given chromosome using plink and converts it to merlin format."
    exit 1
fi

# Chromosome number
chr=$1

# Path prefix of input plink file
input_base=$2

# Output base
output_base=$3

plink --bfile $input_base --noweb --chr $chr --recode --out $output_base
cut -d ' ' -f 6 --complement $output_base.ped > $output_base.merlin.ped
cat $output_base.map | awk '{ print "M", $2 }' > $output_base.merlin.dat
