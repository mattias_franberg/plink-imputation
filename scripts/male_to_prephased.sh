#!/bin/bash

if [ $# -ne 2 ]
then
    echo "Usage: male_to_prephase ped_file prephased_output"
    echo "Converts a .ped for the male X chromosome to a prephased output file."
    exit 1
fi

# The .ped file
ped_file=$1

# The output file
prephased_output=$2

cat $ped_file | awk '{ printf "%s", $1 "->"$2 " HAPLO1 "; for(N=7; N<=NF; N+=2) printf "%s", $N; printf "\n"; printf "%s", $1 "->"$2 " HAPLO2 "; for(N=7; N<=NF; N+=2) printf "%s", $N; printf "\n";}' | gzip -c > $prephased_output
