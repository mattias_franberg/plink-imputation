##
# Author: Anuj Goel
# Modified by: Mattias Frånberg
#
use strict;

use File::Basename;

if ($#ARGV != 3 )
{
    print "Usage: chunkPhasedData.pl merlin_prefix auto_chunk phased_chr output_dir\n";
    print "Splits a phased chromosome into phased chunks.\n";
    exit;
}

# Location of .dat and .ped
my $merlin_prefix = shift;

# Auto chunk file.
my $autochunkfile = shift;
my $auto_chunk_dir = dirname($autochunkfile);

# Location of the phased chromosome.
my $phased_chr = shift;

# Output directory.
my $output_dir = shift;

# The output directory for this chromosome.
my $chr_dir = $output_dir;

# .dat file that contains a list of all SNPs
my $mapfile = "$merlin_prefix.dat";
my $mapname = basename($mapfile);

# Postfix of output phase file (comes after 'chunk-')
my($name, $dir, $ext) = fileparse( $phased_chr, ".gz" );
my $phasefile = $name;

open (AUTOCH, $autochunkfile) or die "Cannot open Autochunk file";
my $blank = <AUTOCH>;
my $header = <AUTOCH>;
my $count = 0;
while (my $line=<AUTOCH>){
  chomp $line;
  my @cells=split (/\t/, $line);
  $count++;
  # Reading chunk file just to check if the first & last SNP name with flag "M" matches with Autochunk's
  open (CHUNK, "$auto_chunk_dir/chunk$count\-$mapname") or die "Cannot open chunk $count";
  my @snpsM=();
  while (my $lin=<CHUNK>){
    chomp $lin;
    my @cellsM=split(" ", $lin);
    #store all SNP names in an array which have a flag "M"
    if ($cellsM[0] eq "M"){
      push (@snpsM, $cellsM[1]);
    }
  }
  close CHUNK;
  #Checking if the START & STOP SNP names in the Autochunk file matches with individual chunk file
  #It should match with the first & last cells of the array to be 100% sure.
  if ($cells[0] eq $snpsM[0] && $cells[1] eq $snpsM[$#snpsM]){
    #MAP file will hold the start & end line numbers. Based on which I will chunk the phase file.
    open (MAP, $mapfile) or die "Cannot open MAP file";
    my $lineStart=0;
    my $lineEnd=0;
    my $linecount=0;
    while (my $map=<MAP>){
      chomp $map;
      #Start line count from 1
      $linecount++;
      my @mapcells=split(/\s+/,$map);
      if ($mapcells[1] eq $cells[0]){
        $lineStart=$linecount;
      }
      if ($mapcells[1] eq $cells[1]){
        $lineEnd=$linecount;
      }
    }
    #get total length of this chunk
    my $length=($lineEnd-$lineStart)+1;
    #print "$cells[0] $lineStart $cells[1] $lineEnd $length\n";
    close MAP;
    open (PHASE, "gunzip -c $phased_chr |") or die "Cannot open phase file";
    open (CHUNKPHASE, "| gzip -c > $output_dir/chunk$count\-$phasefile.gz") or die "Cannot open $count chunk phase file";
    while (my $phase=<PHASE>){
      chomp $phase;
      my @cellsPh=split(/\s+/,$phase);
      my $sub=substr($cellsPh[2],$lineStart-1,$length);
      print CHUNKPHASE "$cellsPh[0] $cellsPh[1] $sub\n";
    }
    close PHASE;
    close CHUNKPHASE;
  }else{
    print "Something went wrong at line $line\n";
  }
}
close AUTOCH;
exit;
