#!/bin/bash

extract_flag=""
sex_flag=""
covar_file=""
pheno_sym="A"
while getopts e:s:c:q flag; do
    case $flag in
        e)
            extract_flag="--remove $OPTARG"
            ;;
        s)
            sex_flag="--filter-$OPTARG"
            ;;
        c)
            covar_file=$OPTARG
            ;;
        q)
            pheno_sym="T"
            ;;
        ?)
            exit;
            ;;
    esac
done

shift $(( OPTIND - 1 ));

if [ $# -ne 3 ]
then
    echo "Usage: to_merlin [-e extract_file] [-s <males|females>] [-c covariates] input_base phenotypes output_base"
    echo "Extracts the given chromosome using plink and converts it to merlin format."
    exit 1
fi

# Path prefix of input plink file
input_base=$1

# Phenotypes
pheno_file=$2

# Output base
output_base=$3

plink_out=$TMPDIR/plink$RANDOM
plink_cov=$plink_out.cov
if [[ $covar_file != "" ]]; then
    plink --bfile $input_base $sex_flag $extract_flag --covar $covar_file --pheno $pheno_file --write-covar --with-phenotype --output-missing-phenotype X --out $plink_out
else
    plink --bfile $input_base $sex_flag $extract_flag --covar $pheno_file --with-phenotype --write-covar --output-missing-phenotype X --out $plink_out
    cat $plink_cov | awk '{ $6 = ""; print }' > $plink_out.tmp 
    mv $plink_out.tmp $plink_cov
fi

tail -n +2 $plink_cov | sed -e 's/\-9\([ \t]\)/X\1/g' | sed -e 's/\-9$/X/' > $output_base.ped
phenotype=`head -n 1 $pheno_file | awk '{ print $3 }'`
echo "$pheno_sym $phenotype" > $output_base.dat

if [[ $covar_file != "" ]]; then
    for covar in `head -n 1 $covar_file`
    do
        if [ $covar == "FID" ] || [ $covar == "IID" ];
        then
            continue
        fi 
        echo "C $covar" >> $output_base.dat
    done
fi
