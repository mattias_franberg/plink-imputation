#!/bin/bash

extract_flag=""
sex_flag=""
while getopts e:s: flag; do
    case $flag in
        e)
            extract_flag="--remove $OPTARG"
            ;;
        s)
            sex_flag="--filter-$OPTARG"
            ;;
        ?)
            exit;
            ;;
    esac
done

shift $(( OPTIND - 1 ));

if [ $# -ne 4 ]
then
    echo "Usage: to_merlin [-e extract_file] [-s <males|females>] input_base cov_pheno covariate_types output_file"
    echo "Extracts the given chromosome using plink and converts it to merlin format."
    exit 1
fi

# Path prefix of input plink file
input_base=$1

# Covariates
covar_file=$2

# Covariate types
covariate_types=$3

# Output file
output_file=$4

plink_out=$TMPDIR/plink$RANDOM
plink --bfile $input_base $sex_flag $extract_flag --covar $covar_file --pheno $pheno_file --write-covar --with-phenotype --output-missing-phenotype X --out $plink_out
phenotype=`head -n 1 $pheno_file | awk '{ print $3 }'`

head -n 1 $plink_out.cov | sed 's/FID/ID_1/' | sed 's/IID/ID_2/' | sed -e s/PHENOTYPE/$phenotype/ | awk '{ $3 = "missing " $3; print }' | cut -f 4-6 -d " " --complement > $output_file
echo "0 0 0 B $covariate_types" >> $output_file
tail -n +2 $plink_out.cov | awk '{ $3 = "0 " $3; print }' | cut -f 4-6 -d " " --complement >> $output_file
