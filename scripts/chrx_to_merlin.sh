#!/bin/bash

if [ $# -ne 2 ]
then
    echo "Usage: chrx_to_merlin input_base output_base"
    echo "Extracts the X chromosome using plink and converts it to merlin format."
    exit 1
fi

# Path prefix of input plink file
input_base=$1

# Output base
output_base=$2

for sex in {males,females}
do
    plink --bfile $input_base --noweb --chr X --filter-$sex --make-bed --out $output_base.$sex.na
    plink --bfile $input_base --noweb --chr 25 --filter-$sex --make-bed --out $output_base.$sex.pa

    if [[ -e $output_base.$sex.na.bim && -e $output_base.$sex.pa.bim ]]
    then
        plink --bfile $output_base.$sex.na --bmerge $output_base.$sex.pa.{bed,bim,fam} --recode --out $output_base.$sex
    
    elif [[ ! -e $output_base.$sex.na.bim && -e $output_base.$sex.pa.bim ]]
    then
        plink --bfile $output_base.$sex.pa --recode --out $output_base.$sex
    elif [[ -e $output_base.$sex.na.bim && ! -e $output_base.$sex.pa.bim ]]
    then
        plink --bfile $output_base.$sex.na --recode --out $output_base.$sex
    else
        continue
    fi

    cut -d ' ' -f 6 --complement $output_base.$sex.ped > $output_base.$sex.merlin.ped
    cat $output_base.$sex.map | awk '{ print "M", $2 }' > $output_base.$sex.merlin.dat
done
