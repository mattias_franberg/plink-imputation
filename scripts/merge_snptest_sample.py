import sys

if len( sys.argv ) != 3:
    print( "Usage: merge_snptest_sample.py file1.sample file2.sample" )
    print( "Merges the two sample files, and outputs the merge in the order of file1." )
    exit( 1 )

def read_header(sample_file, ignore_id = False):
    col_names = sample_file.readline( ).strip( )
    col_types = sample_file.readline( ).strip( )
    
    if ignore_id:
        col_names = ' '.join( col_names.split( )[ 2: ] )
        col_types = ' '.join( col_types.split( )[ 2: ] )
    
    return [ col_names, col_types ]


sample_file1 = open( sys.argv[ 1 ], "r" )
sample_file2 = open( sys.argv[ 2 ], "r" )

header2 = read_header( sample_file2 )
sample_file2_num_cols = len( header2[ 0 ].split( ) ) - 2

iid_line = dict( )
next( sample_file2 )
next( sample_file2 )
for line in sample_file2:
    iid = line.strip( ).split( )[ 1 ]
    iid_line[ iid ] = line.strip( )

try:
    for i in range( 2 ):
        print header2[ i ]

    next( sample_file1 )
    next( sample_file1 )
    for line1 in sample_file1:
        iid = line1.strip( ).split( )[ 1 ]
        line2 = iid_line.get( iid, None )
        if line2:
            print line2
        else:
            sys.stdout.write( "{0} {1}\n".format( ' '.join( line1.strip( ).split( )[ :2 ] ), ' '.join( [ "NA" ] * sample_file2_num_cols ) ) )
except IOError:
    pass
