#!/bin/bash

# Determines which chromosomes to prephase
chr_list=$(seq 1 22)

# Determines whether or not to prephase autosomes
do_auto=1

# Determines whether or not to prephase x-chromosome
do_x=1

# Do QTL analysis instead of case/control
qtl_flag=""

# Ignore covariates
cov_flag=""
while getopts c:r:xaqn flag; do
    case $flag in
        q)
            qtl_flag="-q"
            ;;
        n)
            cov_flag="-c"
            ;;
        c)
            chr_list=$OPTARG
            ;;
        a)
            do_auto=0
            ;;
        x)
            do_x=0
            ;;
        ?)
            exit;
            ;;
    esac
done

shift $(( OPTIND - 1 ));

if [ $# -ne 4 ]
then
    echo "Usage: analyze [-c chr_to_analyze] [-axqn] dose_type pheno_covar_prefix chunk_dir output_dir"
    echo "Analyzes all chunks and outputs the results in the given directory."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

# Type of dose file .prob or .dose
dose_type=$1

# Prefix for .dat and .ped files
pheno_covar_prefix=$2

# Directory containing all chunks
chunk_dir=$3

# Output dir
output_dir=$4

if [ $do_auto == 1 ]; then
    for chr in $chr_list;
    do
        input_chr_dir=$chunk_dir/chr/$chr
        output_chr_dir=$output_dir/chr/$chr
        mkdir -p $output_chr_dir
        
        for chunk in $input_chr_dir/chunk*-chr$chr.imputed.$dose_type.gz;
        do
            chunk_prefix=${chunk%.*.*}

            chunk_name=$(basename "$chunk")
            chunk_name=${chunk_name%%.*}

            sbatch $IMP_DIR/analyze_chunk_mach.sh $qtl_flag $cov_flag $dose_type $pheno_covar_prefix $chunk_prefix $output_chr_dir/$chunk_name.out
        done
    done
fi

if [ $do_x == 1 ]; then
    # X chromosome
    chr="X"
    for chr_type in {males.auto,males.no.auto,females.auto,females.no.auto}
    do
        input_chr_dir=$transposed_dir/chr/$chr
        output_chr_dir=$output_dir/chr/$chr
        mkdir -p $output_chr_dir
        
        num_chunks=$( find $input_chr_dir -name "chunk*-chr$chr.$chr_type.imputed.$dose_type.gz" | wc -l )
        if [ $num_chunks -gt 0 ]; then
            for chunk in $input_chr_dir/chunk*-chr$chr.$chr_type.imputed.$dose_type.gz;
            do
                chunk_prefix=${chunk%.*.*}

                chunk_name=$(basename "$chunk")
                chunk_name=${chunk_name%%.*}
                
                sbatch $IMP_DIR/analyze_chunk_mach.sh $qtl_flag $cov_flag $dose_type $pheno_covar_prefix $chunk_prefix $output_chr_dir/$chunk_name.out
            done
        fi
    done
fi
