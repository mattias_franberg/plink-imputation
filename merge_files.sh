#!/bin/bash

if [ $# -eq 0 ]
then
    echo "Usage: merge_files [file1 file2 file3 ...]"
    echo "Merges the given files with headers into a file with only one header."
    exit 1
fi

file_list=$*
zcat ${file_list[0]} | head -n 1 | gzip -c
for file in $file_list;
do
    zcat $file | tail -n +2 | gzip -c
done
