#!/bin/bash

# Determines which chromosomes to prephase
chr_list=$(seq 1 22)

# Determines whether or not to prephase autosomes
do_auto=1

# Determines whether or not to prephase x-chromosome
do_x=1
while getopts c:xa flag; do
    case $flag in
        c)
            chr_list=$OPTARG
            ;;
        a)
            do_auto=0
            ;;
        x)
            do_x=0
            ;;
        ?)
            exit;
            ;;
    esac
done

shift $(( OPTIND - 1 ));

if [ $# -ne 4 ]
then
    echo "Usage: prephase [-c list_of_chr_to_analyze] [-ax] input_plink_base chunk_length chunk_overlap output_dir"
    echo "Prephases all chromosomes and outputs the results in the given directory."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

# Path prefix to the plink input files
plink_base=$1

# Number of SNPs in a chunk
length=$2

# Number of overlapping SNPs between chunks
overlap=$3

# Output directory
output_dir=$4

# Autosomes
if [ $do_auto == 1 ]; then
    for chr in $chr_list;
    do
        chr_dir=$output_dir/chr/$chr
        mkdir -p $chr_dir

        # Convert to merlin format and chunk chromosome
        $IMP_DIR/scripts/chr_to_merlin.sh $chr $plink_base $chr_dir/chr$chr
        pushd $chr_dir
        ChunkChromosome -d ./chr$chr.merlin.dat -n $length -o $overlap
        popd
        
        # Prephase each chunk as its own job
        merlin_prefix=$chr_dir/chr$chr.merlin
        for chunk in $chr_dir/chunk*.merlin.dat;
        do
            filename=$(basename $chunk)
            prefix=${filename%.*}
       
            sbatch $IMP_DIR/prephase_chunk.sh $chunk $merlin_prefix.ped $chr_dir/$prefix
        done
    done
fi


# X chromosome
if [ $do_x == 1 ]; then
    chr="X"
    chr_dir=$output_dir/chr/$chr
    mkdir -p $chr_dir

    # Split x chromosome into male and female merlin files
    $IMP_DIR/scripts/chrx_to_merlin.sh $plink_base $chr_dir/chr$chr

    merlin_prefix=$chr_dir/chr$chr.females.merlin
    if [ -e $merlin_prefix.dat ]; then

        # Convert to merlin format and chunk chromosome
        pushd $chr_dir
        ChunkChromosome -d ./chr$chr.females.merlin.dat -n $length -o $overlap
        popd
        
        # Prephase each chunk as its own job for females
        merlin_prefix=$chr_dir/chr$chr.females.merlin
        for chunk in $chr_dir/chunk*.females.merlin.dat;
        do
            filename=$(basename $chunk)
            prefix=${filename%.*}
       
            sbatch $IMP_DIR/prephase_chunk.sh $chunk $merlin_prefix.ped $chr_dir/$prefix
        done
    fi

    merlin_prefix=$chr_dir/chr$chr.males.merlin
    if [ -e $merlin_prefix.dat ]; then
        # Convert to merlin format and chunk chromosome
        pushd $chr_dir
        ChunkChromosome -d ./chr$chr.males.merlin.dat -n $length -o $overlap
        popd

        # Prephase and chunk the prephased data, males are already prephased
        $IMP_DIR/scripts/male_to_prephased.sh $merlin_prefix.ped $merlin_prefix.gz

        # Chunk the prephased data
        auto_chunk=$chr_dir/autoChunk-chr$chr.males.merlin.dat
        perl $IMP_DIR/scripts/chunkPhasedData.pl $merlin_prefix $auto_chunk $merlin_prefix.gz $chr_dir
    fi
fi
