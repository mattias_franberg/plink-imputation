#!/bin/bash

if [ $# -ne 5 ]
then
    echo "Usage: transpose type alias_file order_file input_dir output_dir"
    echo "Transposes all the chunks in the given directory, the type is 'prob' or 'dose'."
    exit 1
fi

if [ ! -d "$IMP_DIR" ]
then
    echo "error: IMP_DIR not set."
    exit 1
fi

# Dose type 'prob' or 'dose'
dose_type=$1

# Alias file
alias_file=$2

# Order file
order_file=$3

# Input directory
input_dir=$4

# Output directory 
output_dir=$5

for chr in {1..22};
do
    input_chr_dir=$input_dir/chr/$chr
    output_chr_dir=$output_dir/chr/$chr
    mkdir -p $output_chr_dir

    for chunk in $input_chr_dir/chunk*-chr$chr.imputed.$dose_type.gz;
    do
        chunk_name=$(basename $chunk)
        chunk_name=${chunk_name%.*.*}

        chunk_prefix=${chunk%.*.*}

        info_file=$chunk_prefix.info.gz #chrpos.info.gz
        sbatch $IMP_DIR/transpose_chunk.sh $dose_type $alias_file $order_file $chunk $info_file $output_chr_dir/$chunk_name.transposed.$dose_type.gz
    done
done

# X chromosome
chr='X'
for xtype in {males.auto,males.no.auto,females.auto,females.no.auto};
do
    input_chr_dir=$input_dir/chr/$chr
    output_chr_dir=$output_dir/chr/$chr
    mkdir -p $output_chr_dir

    chunk_list=($input_chr_dir/chunk*-chr$chr.$xtype.imputed.$dose_type.gz)
    if [ ! -f ${chunk_list[0]} ];
    then
        continue
    fi

    for chunk in ${chunk_list[@]};
    do
        chunk_name=$(basename $chunk)
        chunk_name=${chunk_name%.*.*}

        chunk_prefix=${chunk%.*.*}

        info_file=$chunk_prefix.chrpos.info.gz
        sbatch $IMP_DIR/transpose_chunk.sh $dose_type $alias_file $order_file $chunk $info_file $output_chr_dir/$chunk_name.transposed.$dose_type.gz
    done
done
